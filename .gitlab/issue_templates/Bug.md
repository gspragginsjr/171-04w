# URL(s) Affected
- https://example.160over90.io/locations/

# Browser(s) and Version(s) Affected
- Safari 11.1

# Operating System(s) Affected
- Example: macos 10.13.3

# Device(s) affected
- MacBook Pro (15-inch, 2017)

# Steps to reproduce

1. Scroll down to the map.
2. Click any dot on the map.
3. Click "Learn More" in the popup window.

# What is expected?

The browser should open the link in a new tab.

# What actually is happening?

The browser opens the link in the same tab.

# Any additional comments? (optional)
