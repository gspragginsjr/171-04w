// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'babel-polyfill';
import Vue from 'vue';
import VueTouch from 'vue-touch';
import svg4everybody from 'svg4everybody';

import App from '@/components/App';
import intersect from '@/js/directives/intersect';

// Header
import GlobalHeader from '@/components/header/GlobalHeader';

// Fade Slider
import FadeSlider from '@/components/fadeslider/FadeSlider';

// Modal
import ModalButton from '@/components/modal/ModalButton';
import ModalBackdrop from '@/components/modal/ModalBackdrop';
import ModalBackdropDialog from '@/components/modal/ModalBackdropDialog';

// Disclosure
import DisclosureButton from '@/components/disclosure/DisclosureButton';
import DisclosureContainer from '@/components/disclosure/DisclosureContainer';

// Accordion
import AccordionContainer from '@/components/accordion/AccordionContainer';
import AccordionContainerPanel from '@/components/accordion/AccordionContainerPanel';
import AccordionContainerHeader from '@/components/accordion/AccordionContainerHeader';
import AccordionContainerHeaderButton from '@/components/accordion/AccordionContainerHeaderButton';

// Tabs
import TabsContainer from '@/components/tabs/TabsContainer';
import TabsContainerTablist from '@/components/tabs/TabsContainerTablist';
import TabsContainerTabpanel from '@/components/tabs/TabsContainerTabpanel';
import TabsContainerTablistTab from '@/components/tabs/TabsContainerTablistTab';

// Registration
import RegistrationForm from '@/components/registration/RegistrationForm';

// Vimeo
import VimeoButton from '@/components/vimeo/VimeoButton';
import VimeoPlayer from '@/components/vimeo/VimeoPlayer';
import VimeoContainer from '@/components/vimeo/VimeoContainer';

// Video
import VideoPlayer from '@/components/video/VideoPlayer';

// Carousel
import CarouselNav from '@/components/carousel/CarouselNav';
import CarouselItem from '@/components/carousel/CarouselItem';
import CarouselButton from '@/components/carousel/CarouselButton';
import CarouselContainer from '@/components/carousel/CarouselContainer';

// Carousel 2
import GlobalCarousel from '@/components/carousel2/GlobalCarousel';
import GlobalCarouselDot from '@/components/carousel2/GlobalCarouselDot';

// Filter
import GlobalFilter from '@/components/filter/GlobalFilter';
import GlobalFilterButton from '@/components/filter/GlobalFilterButton';
import GlobalFilterItemsWrap from '@/components/filter/GlobalFilterItemsWrap';
import GlobalFilterItem from '@/components/filter/GlobalFilterItem';
import GlobalFilterItemGhost from '@/components/filter/GlobalFilterItemGhost';
import GlobalFilterCheckbox from '@/components/filter/GlobalFilterCheckbox';

// Form Fields
import BaseControl from '@/components/form-fields/BaseControl';

// Transitions
import TransitionSlide from '@/components/transitions/TransitionSlide';

// Favorites
import GlobalFavoritesButton from '@/components/favorites/GlobalFavoritesButton';
import GlobalFavoritesOn from '@/components/favorites/GlobalFavoritesOn';

Vue.config.productionTip = false;

Vue.directive('intersect', intersect);

Vue.use(VueTouch, { name: 'v-touch' });

require('es6-promise').polyfill();

svg4everybody();

// Header
Vue.component('GlobalHeader', GlobalHeader);

// Fade Slider
Vue.component('FadeSlider', FadeSlider);

// Modal
Vue.component('ModalButton', ModalButton);
Vue.component('ModalBackdrop', ModalBackdrop);
Vue.component('ModalBackdropDialog', ModalBackdropDialog);

// Disclosure
Vue.component('DisclosureButton', DisclosureButton);
Vue.component('DisclosureContainer', DisclosureContainer);

// Accordion
Vue.component('AccordionContainer', AccordionContainer);
Vue.component('AccordionContainerPanel', AccordionContainerPanel);
Vue.component('AccordionContainerHeader', AccordionContainerHeader);
Vue.component('AccordionContainerHeaderButton', AccordionContainerHeaderButton);

// Tabs
Vue.component('TabsContainer', TabsContainer);
Vue.component('TabsContainerTablist', TabsContainerTablist);
Vue.component('TabsContainerTabpanel', TabsContainerTabpanel);
Vue.component('TabsContainerTablistTab', TabsContainerTablistTab);

// Registration
Vue.component('RegistrationForm', RegistrationForm);

// Vimeo Video
Vue.component('VimeoButton', VimeoButton);
Vue.component('VimeoPlayer', VimeoPlayer);
Vue.component('VimeoContainer', VimeoContainer);

// Video
Vue.component('VideoPlayer', VideoPlayer);

// Carousel
Vue.component('CarouselNav', CarouselNav);
Vue.component('CarouselItem', CarouselItem);
Vue.component('CarouselButton', CarouselButton);
Vue.component('CarouselContainer', CarouselContainer);

// Carousel 2
Vue.component('GlobalCarousel', GlobalCarousel);
Vue.component('GlobalCarouselDot', GlobalCarouselDot);

// Filter
Vue.component('GlobalFilter', GlobalFilter);
Vue.component('GlobalFilterButton', GlobalFilterButton);
Vue.component('GlobalFilterItemsWrap', GlobalFilterItemsWrap);
Vue.component('GlobalFilterItem', GlobalFilterItem);
Vue.component('GlobalFilterItemGhost', GlobalFilterItemGhost);
Vue.component('GlobalFilterCheckbox', GlobalFilterCheckbox);

// Form Fields
Vue.component('BaseControl', BaseControl);

// Transitions
Vue.component('TransitionSlide', TransitionSlide);

// Favorites
Vue.component('GlobalFavoritesButton', GlobalFavoritesButton);
Vue.component('GlobalFavoritesOn', GlobalFavoritesOn);

/* eslint-disable no-new */
new Vue(App).$mount('#app');
