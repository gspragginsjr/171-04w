const { browserslist } = require('./package.json');

module.exports = {
    processors: ['stylelint-processor-html'],
    extends: 'stylelint-config-primer',
    rules: {
        indentation: 4,
        'plugin/no-unsupported-browser-features': [
            true,
            {
                severity: 'warning',
                browsers: browserslist,
                ignore: [
                    'object-fit',
                    'multicolumn',
                    'viewport-units',
                    'css-appearance',
                    'font-unicode-range',
                    'css3-cursors-newer',
                    'flexbox',
                    'will-change',
                    'outline',
                    'transforms3d'
                ],
            },
        ],
        'scss/at-rule-no-unknown': true,
    },
};
